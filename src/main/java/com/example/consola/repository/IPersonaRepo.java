package com.example.consola.repository;

public interface IPersonaRepo {
    //programar orienda a interfacez porque permite acoplar el codigo
    void registrar(String nombre);
}
